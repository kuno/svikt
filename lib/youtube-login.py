#!/bin/env python3

from os.path import abspath,dirname,join,realpath
import codecs
import json
import requests
import time
import xdg.BaseDirectory

app_data_path = xdg.BaseDirectory.save_data_path ('svikt')

def save_json (name, data):
    filename = join (app_data_path, name + '.json')

    with codecs.open (filename, 'wb', 'utf-8') as f:
        f.write (json.dumps (data, indent=4) + "\n")


def load_config ():
    config_json = join (dirname (dirname (realpath (__file__))), 'client_id.json')

    with codecs.open (config_json, 'rb', 'utf-8') as f:
        data = json.loads (f.read ())

        return data['installed']


def login (config):
    auth_url = 'https://accounts.google.com/o/oauth2/device/code'

    parameters = {
        'client_id': config['client_id'],
#        'scope': 'https://www.googleapis.com/auth/youtube.upload'
        'scope': 'https://www.googleapis.com/auth/youtube'
    }

    response = requests.post (auth_url, data=parameters)

    verify = response.json()
    save_json ('verify', verify)

    print ('Visit:', verify['verification_url'])
    print ('')
    print ('And enter this code:')
    print ('')
    print ('        ', verify['user_code'])
    print ('')

    token_url = 'https://accounts.google.com/o/oauth2/token'

    authorized = False
    while not authorized:
        parameters = {
            'client_id': config['client_id'],
            'client_secret': config['client_secret'],
            'code': verify['device_code'],
            'grant_type': 'http://oauth.net/grant_type/device/1.0',
        }
        response = requests.post (token_url, data=parameters)
        authorization = response.json()

        if ("error" in authorization):
            print ("Still waiting, server response:", authorization["error"])
            time.sleep (verify['interval'] + 1)
        else:
            authorized = True
            save_json ('authorization', authorization)

login (load_config ())
