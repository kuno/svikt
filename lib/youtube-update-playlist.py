#!/bin/env python3

from os.path import abspath,dirname,join,realpath
import codecs
import http.client
import json
import logging
import requests
import sys
import time
import xdg.BaseDirectory

app_data_path = xdg.BaseDirectory.save_data_path ('svikt')

def debug ():
    http.client.HTTPConnection.debuglevel = 1

    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True


def save_json (name, data):
    filename = join (app_data_path, name + '.json')

    with codecs.open (filename, 'wb', 'utf-8') as f:
        f.write (json.dumps (data, indent=4) + "\n")


def load_config ():
    config_json = join (dirname (dirname (realpath (__file__))), 'client_id.json')

    with codecs.open (config_json, 'rb', 'utf-8') as f:
        data = json.loads (f.read ())

        return data['installed']


def load_authorization ():
    config_json = join (app_data_path, 'authorization.json')

    with codecs.open (config_json, 'rb', 'utf-8') as f:
        return json.loads (f.read ())


def save_authorization (authorization):
    save_json ('authorization', authorization)


def refresh_token (authorization):
    config = load_config ()

    parameters = {
        'client_id': config['client_id'],
        'client_secret': config['client_secret'],
        'refresh_token': authorization['refresh_token'],
        'grant_type': 'refresh_token'
    }

    token_url = 'https://accounts.google.com/o/oauth2/token'
    response = requests.post (token_url, data=parameters)

    print ("Refresh token response:", response.status_code)
    new_authorization = response.json()
    import pprint
    pprint.pprint (new_authorization)

    authorization['expires_in'] = new_authorization['expires_in']
    authorization['access_token'] = new_authorization['access_token']

    save_authorization (authorization)
    return authorization


def youtube_request (authorization, url, params):
    headers = {
        "Authorization": "Bearer " + authorization['access_token']
    }

    response = requests.get(url, headers=headers, params=params)
    if response.status_code == 401:
        print ("Not authorized, log in first")
        sys.exit (1)

    if response.status_code == 403:
        print ("Response:", response.status_code)

        authorization = refresh_token (authorization)
        headers["Authorization"] = "Bearer " + authorization['access_token']
        response = requests.get(url, headers=headers, params=params)

    print ("Response:", response.status_code)
    import pprint
    pprint.pprint (response.json())


# debug ()

authorization = load_authorization ()
url = 'https://www.googleapis.com/youtube/v3/playlists'
params = {
    "part": "snippet",
    "mine": "true"
}
youtube_request (authorization, url, params)
